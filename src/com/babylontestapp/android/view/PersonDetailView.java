package com.babylontestapp.android.view;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.babylontestapp.android.R;


/**
 * View class for the detail screen.
 * @author Balazs Szabo
 */
public class PersonDetailView {

	private final TextView firstNameLabel;
	private final TextView surnameLabel;
	private final TextView addressLabel;
	private final TextView phoneNumberLabel;
	private final TextView emailLabel;
	private final TextView createdAtLabel;
	private final TextView updatedAtLabel;
	
	private final TextView firstName;
	private final TextView surname;
	private final TextView address;
	private final TextView phoneNumber;
	private final TextView email;
	private final TextView createdAt;
	private final TextView updatedAt;
	
	public PersonDetailView(View view) {
		firstNameLabel = (TextView) view.findViewById(R.id.firstname_label);
		surnameLabel = (TextView) view.findViewById(R.id.surname_label);
		addressLabel = (TextView) view.findViewById(R.id.address_label);
		phoneNumberLabel = (TextView) view.findViewById(R.id.phone_number_label);
		emailLabel = (TextView) view.findViewById(R.id.email_label);
		createdAtLabel = (TextView) view.findViewById(R.id.createdat_label);
		updatedAtLabel = (TextView) view.findViewById(R.id.updatedat_label);
		
		firstName = (TextView) view.findViewById(R.id.first_name);
		surname = (TextView) view.findViewById(R.id.surname);
		address = (TextView) view.findViewById(R.id.address);
		phoneNumber = (TextView) view.findViewById(R.id.phone_number);
		email = (TextView) view.findViewById(R.id.email);
		createdAt = (TextView) view.findViewById(R.id.createdat);
		updatedAt = (TextView) view.findViewById(R.id.updatedat);
	}
	
	public TextView getFirstNameLabel() {
		return firstNameLabel;
	}

	public TextView getSurnameLabel() {
		return surnameLabel;
	}

	public TextView getAddressLabel() {
		return addressLabel;
	}

	public TextView getPhoneNumberLabel() {
		return phoneNumberLabel;
	}

	public TextView getEmailLabel() {
		return emailLabel;
	}

	public TextView getCreatedAtLabel() {
		return createdAtLabel;
	}

	public TextView getUpdatedAtLabel() {
		return updatedAtLabel;
	}

	public TextView getFirstName() {
		return firstName;
	}

	public TextView getSurname() {
		return surname;
	}

	public TextView getAddress() {
		return address;
	}

	public TextView getPhoneNumber() {
		return phoneNumber;
	}

	public TextView getEmail() {
		return email;
	}

	public TextView getCreatedAt() {
		return createdAt;
	}

	public TextView getUpdatedAt() {
		return updatedAt;
	}


}
