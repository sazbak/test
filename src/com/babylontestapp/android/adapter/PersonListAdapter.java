package com.babylontestapp.android.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylontestapp.android.R;
import com.babylontestapp.android.database.PersonDbModel;
import com.squareup.picasso.Picasso;


/**
 * Adapter for the {@see PersonListFragment}.
 * @author Balazs Szabo
 */
public class PersonListAdapter extends ArrayAdapter<PersonDbModel> {
	
	private static final String BASE_URL = "http://api.adorable.io/avatar/";
	private static final String AVATAR_SIZE = "50/";
	
	private ArrayList<PersonDbModel> persons;
	private LayoutInflater inflater;
	private Context context;

	public PersonListAdapter(Context context, int resource, ArrayList<PersonDbModel> objects) {
		super(context, resource, objects);
		this.context = context;
		this.persons = objects;
		inflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}
	
	static class ViewHolder {
		TextView firstName;
		TextView surname;
		ImageView avatar;
	}
	
	public PersonDbModel getItem(int position) {
		return persons.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listelement, null);
			holder = new ViewHolder();
			holder.firstName = (TextView) convertView.findViewById(R.id.list_element_firstname);
			holder.surname = (TextView) convertView.findViewById(R.id.list_element_surname);
			holder.avatar = (ImageView) convertView.findViewById(R.id.list_element_avatar);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		holder.firstName.setText(persons.get(position).getFirst_name());
		holder.surname.setText(persons.get(position).getSurname());
		Picasso.with(context).load(
				BASE_URL + AVATAR_SIZE + persons.get(position).getEmail()).into(holder.avatar);
		

		return convertView;
	}


}
