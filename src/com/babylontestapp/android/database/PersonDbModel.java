package com.babylontestapp.android.database;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.babylontestapp.android.network.model.Person;
import com.orm.SugarRecord;


/**
 * Model class for saving a person's data to a database.
 * @author Balazs Szabo
 */
public class PersonDbModel extends SugarRecord<PersonDbModel> implements Parcelable {

	private String first_name;
	private String surname;
	private String address;
	private String phone_number;
	private String email;
	private Integer id;
	private Date createdAt;
	private Date updatedAt;
	
	public PersonDbModel() {
		
	}
	
	public PersonDbModel(Person person) {
		first_name = person.getFirst_name();
		surname = person.getSurname();
		address = person.getAddress();
		phone_number = person.getPhone_number();
		email = person.getEmail();
		id = person.getPersonId();
		createdAt = person.getCreatedAt();
		updatedAt = person.getUpdatedAt();
	}
	
	protected PersonDbModel(Parcel in) {
		first_name = in.readString();
		surname = in.readString();
		address = in.readString();
		phone_number = in.readString();
		email = in.readString();
		id = in.readByte() == 0x00 ? null : in.readInt();
		long tmpCreatedAt = in.readLong();
		createdAt = tmpCreatedAt != -1 ? new Date(tmpCreatedAt) : null;
		long tmpUpdatedAt = in.readLong();
		updatedAt = tmpUpdatedAt != -1 ? new Date(tmpUpdatedAt) : null;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(first_name);
		dest.writeString(surname);
		dest.writeString(address);
		dest.writeString(phone_number);
		dest.writeString(email);
		if (id == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(id);
		}
		dest.writeLong(createdAt != null ? createdAt.getTime() : -1L);
		dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1L);
	}

	public static final Parcelable.Creator<PersonDbModel> CREATOR = new Parcelable.Creator<PersonDbModel>() {
		@Override
		public PersonDbModel createFromParcel(Parcel in) {
			return new PersonDbModel(in);
		}

		@Override
		public PersonDbModel[] newArray(int size) {
			return new PersonDbModel[size];
		}
	};	

	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getPersonId() {
		return id;
	}
	public void setPersonId(Integer id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
