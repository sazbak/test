package com.babylontestapp.android.database;

import java.util.ArrayList;
import java.util.List;

import com.babylontestapp.android.network.model.Person;

/**
 * This class contains methods for handling database records.
 * @author Balazs Szabo
 */
public class DatabaseHandler {

	public ArrayList<PersonDbModel> personToPersonDb(ArrayList<Person> persons) {

		ArrayList<PersonDbModel> personsDb = new ArrayList<PersonDbModel>();

		for(Person person : persons) {
			PersonDbModel personDbModel = new PersonDbModel(person);
			personsDb.add(personDbModel);
		}

		return personsDb;
	}

	public void saveToDb(ArrayList<PersonDbModel> personsDb) {
		PersonDbModel.saveInTx(personsDb);
	}
	
	public List<PersonDbModel> getFromDb() {
		return PersonDbModel.listAll(PersonDbModel.class);
	}

}
