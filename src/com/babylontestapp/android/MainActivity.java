package com.babylontestapp.android;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.babylontestapp.android.database.DatabaseHandler;
import com.babylontestapp.android.database.PersonDbModel;
import com.babylontestapp.android.fragment.PersonListFragment;
import com.babylontestapp.android.network.PersonsService;
import com.babylontestapp.android.network.model.Person;


/**
 * Main entry for the app.
 * @author Balazs Szabo
 */
public class MainActivity extends FragmentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new PersonsService().getApiService().getPersons(new Callback<ArrayList<Person>>() {

			@Override
			public void success(ArrayList<Person> persons, Response resp) {
				DatabaseHandler dbHandler = new DatabaseHandler();
				ArrayList<PersonDbModel> personsDb = dbHandler.personToPersonDb(persons);
				dbHandler.saveToDb(personsDb);
				getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, PersonListFragment.newInstance(personsDb)).commit();
			}

			@Override
			public void failure(RetrofitError error) {
				ArrayList<PersonDbModel> personsFromDb = (ArrayList<PersonDbModel>)new DatabaseHandler().getFromDb();
				if(personsFromDb.isEmpty()) {
					Toast.makeText(MainActivity.this, getString(R.string.no_connection_toast_msg), Toast.LENGTH_SHORT)
					.show();
				} else {
					getSupportFragmentManager().beginTransaction()
	                .add(R.id.fragment_container, PersonListFragment.newInstance(personsFromDb)).commit();
				}
			}
		});
	}
}
