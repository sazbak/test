package com.babylontestapp.android.network;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;

import com.babylontestapp.android.network.model.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

/**
 * Responsible for getting the persons data with a network call.
 * @author Balazs Szabo
 */
public class PersonsService {

	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String BASE_URL = "http://fast-gorge.herokuapp.com";

	private PersonsServiceEndpointInterface apiService;

	public PersonsService() {
		Gson gson = new GsonBuilder()
		.setDateFormat(DATE_FORMAT)
		.create();
		RestAdapter restAdapter = new RestAdapter.Builder()
		.setEndpoint(BASE_URL)
		.setConverter(new GsonConverter(gson))
		.build();
		apiService = restAdapter.create(PersonsServiceEndpointInterface.class);
	}

	public interface PersonsServiceEndpointInterface {

		@GET("/contacts")
		void getPersons(Callback<ArrayList<Person>> persons);
	}
	
	public PersonsServiceEndpointInterface getApiService() {
		return apiService;
	}


}
