package com.babylontestapp.android.network.model;

import java.util.Date;

import com.babylontestapp.android.database.PersonDbModel;

/**
 * People's Json data is read into this.
 * @author Balazs Szabo
 */
public class Person {

	private String first_name;
	private String surname;
	private String address;
	private String phone_number;
	private String email;
	private Integer id;
	private Date createdAt;
	private Date updatedAt;
	
	public Person(PersonDbModel personDb) {
			first_name = personDb.getFirst_name();
			surname = personDb.getSurname();
			address = personDb.getAddress();
			phone_number = personDb.getPhone_number();
			email = personDb.getEmail();
			id = personDb.getPersonId();
			createdAt = personDb.getCreatedAt();
			updatedAt = personDb.getUpdatedAt();	
	}

	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getPersonId() {
		return id;
	}
	public void setPersonId(Integer id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
