package com.babylontestapp.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.babylontestapp.android.R;
import com.babylontestapp.android.database.PersonDbModel;
import com.babylontestapp.android.view.PersonDetailView;


/**
 * Fragment for the detailed personal data.
 * @author Balazs Szabo
 */
public class PersonDetailFragment extends Fragment {
	
	private static final String SELECTED_PERSON= "SELECTED_PERSON";
	
	private PersonDbModel person;
	private PersonDetailView personDetailView;
	
	public static PersonDetailFragment newInstance(PersonDbModel selectedPerson) {
		PersonDetailFragment personFragment = new PersonDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable(SELECTED_PERSON, selectedPerson);
		personFragment.setArguments(bundle);
		return personFragment;
	}
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		person = getArguments().getParcelable(SELECTED_PERSON);
		View view = inflater.inflate(R.layout.persondetails, container, false);
		setupViews(view);
		return view;
	}
	
	private void setupViews(View view) {
		personDetailView = new PersonDetailView(view);
		personDetailView.getFirstName().setText(person.getFirst_name());
		personDetailView.getSurname().setText(person.getSurname());;
		personDetailView.getAddress().setText(person.getAddress());
		personDetailView.getPhoneNumber().setText(person.getPhone_number());
		personDetailView.getEmail().setText(person.getEmail());
		personDetailView.getCreatedAt().setText(person.getCreatedAt().toString());
		personDetailView.getUpdatedAt().setText(person.getUpdatedAt().toString());
	}

}
