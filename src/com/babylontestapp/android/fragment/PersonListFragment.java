package com.babylontestapp.android.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.babylontestapp.android.R;
import com.babylontestapp.android.adapter.PersonListAdapter;
import com.babylontestapp.android.database.PersonDbModel;


/**
 * ListFragment to show the data of the persons in a list format.
 * @author Balazs Szabo
 */
public class PersonListFragment extends ListFragment {

	private static final String PERSON_LIST = "PERSON_LIST";

	public static PersonListFragment newInstance(ArrayList<PersonDbModel> persons) {
		PersonListFragment listFragment = new PersonListFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelableArrayList(PERSON_LIST, persons);
		listFragment.setArguments(bundle);
		return listFragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final ArrayList<PersonDbModel> persons = getArguments().getParcelableArrayList(PERSON_LIST);
		PersonListAdapter personListAdapter = new PersonListAdapter(
				getActivity(), android.R.layout.simple_list_item_1, persons);
		setListAdapter(personListAdapter);
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, 
						PersonDetailFragment.newInstance(
								persons.get(position))).addToBackStack(null).commit();
			}
		});
	}
}
